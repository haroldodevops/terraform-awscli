FROM mcr.microsoft.com/azure-cli
#FROM alpine:3.9.5

#Terraform with awscli
RUN apk add --no-cache \
	ca-certificates \
        openssh-client \
        git \
        bash \
        openssh \
	groff \
        zip \
        less \
        python \
        py-pip \
        wget \ 	
        && rm -rf /var/cache/apk/* \
  && pip install pip --upgrade \
  && pip install awscli 
  
  
RUN ssh-keygen -C haroldo.silva@yaman.com.br -f ~/.ssh/id_rsa -P 123456

RUN  wget https://releases.hashicorp.com/terraform/0.12.24/terraform_0.12.24_linux_amd64.zip \
     && unzip terraform_0.12.24_linux_amd64.zip \
     && mv terraform /usr/local/bin/ \
     && rm terraform_0.12.24_linux_amd64.zip
